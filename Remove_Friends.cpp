#include <iostream>
#include <stack>

#define TEST_CASE_LOWER_LIMIT 0
#define TEST_CASE_UPPER_LIMIT 1000
#define TOTAL_FRIENDS_LOWER_LIMIT 0
#define TOTAL_FRIENDS_UPPER_LIMIT 100000
#define FRIENDS_TO_DELETE_LOWER_LIMIT 0

using namespace std;
int friends_to_delete;
stack <int> friends_stack;
void print_stack(stack <int> friend_stack);
void remove_friends_during_insertion(int friend_popularity);
bool is_valid_input(int test_cases,int total_friends,int friends_to_delete);
void remove_friends_after_insertion();
void reverse_stack();

int main(int argc, char const *argv[])
{
	
	int test_cases,total_friends;
		
	cin>>test_cases;

	for (int i = 0; i < test_cases; ++i)
	{
		cin>>total_friends;
		cin>>friends_to_delete;

		if(is_valid_input(test_cases,total_friends,friends_to_delete))
		{
			for (int i = 0; i < total_friends; ++i)
			{
				int friend_popularity;
				cin>>friend_popularity;
				remove_friends_during_insertion(friend_popularity);
				friends_stack.push(friend_popularity);
			}
			
			remove_friends_after_insertion();
			reverse_stack();	
		}
	}
	return 0;
}

void reverse_stack()
{
	stack<int> temporary_stack;
	while(!friends_stack.empty())
	{
		temporary_stack.push(friends_stack.top());
		friends_stack.pop();
	}
	print_stack(temporary_stack);
}

void print_stack(stack <int> friend_stack)
{
    stack <int> local_stack = friend_stack;
    while (!local_stack.empty())
    {
        cout << ' ' << local_stack.top();
        local_stack.pop();
    }
    cout << '\n';
}

void remove_friends_during_insertion(int friend_popularity)
{
	while(friends_stack.size()>0 && friends_stack.top() < friend_popularity && friends_to_delete > 0 )
	{
		friends_stack.pop();
		friends_to_delete--;
	}

}

bool is_valid_input(int test_cases,int total_friends,int friends_to_delete)
{
	return((test_cases > TEST_CASE_LOWER_LIMIT && test_cases <= TEST_CASE_UPPER_LIMIT) && 
			(total_friends > TOTAL_FRIENDS_LOWER_LIMIT && total_friends <= TOTAL_FRIENDS_UPPER_LIMIT) && 
				(friends_to_delete >= FRIENDS_TO_DELETE_LOWER_LIMIT && friends_to_delete < total_friends));
}

void remove_friends_after_insertion()
{
	while(friends_to_delete > 0)
	{
		friends_stack.pop();																																																																																																																																																																																																																																																																																																																																																																																																																								
	}
}