package javaapplication2;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

class MonkAndChamberOfSecretsSolution
{

    private static int numberOfSpiders, spidersToSelect, spidersToVisit;
    private Spider temporaryArray[];
    static private MonkAndChamberOfSecretsSolution monk;
    static private Queue<Spider> queue;
    static int tempIndex;                                          //temporarily holdes the index of spiders having max value in queue

    public void getSpiderData() throws Exception
    {
        Scanner in = new Scanner(System.in);
        numberOfSpiders = in.nextInt();
        spidersToSelect = in.nextInt();
        //Constraints Checking    
        if (!isValidNumberOfSpiders(numberOfSpiders, spidersToSelect))
        {
            throw new Exception("Invalid Spider Numbers");
        }
        spidersToVisit = spidersToSelect;
        temporaryArray = new Spider[spidersToSelect];
        // Initialize Queue with user value 
        for (int index = 1; index <= numberOfSpiders; index++)
        {
            queue.add(new Spider(in.nextInt(), index));
            if (!isValidPower(queue.element().getPower()))
            {
                throw new Exception("Invalid Power");
            }
        }
    }

    public int getNumberOfSpidersToSelect()
    {
        return queue.size() < spidersToVisit ? queue.size() : spidersToVisit;
    }

    public int findMaxSpiderPower(Spider temporaryArray[])
    {
        int max = -1, index = 0, arrayIndex = 0;
        for (int currentPosition = 0; currentPosition < spidersToVisit; currentPosition++)
        {
            if (temporaryArray[currentPosition].getPower() > max)
            {
                max = temporaryArray[currentPosition].getPower();
                index = temporaryArray[currentPosition].getIndex();
                arrayIndex = currentPosition;
            }
        }
        System.out.print(index + " ");
        return arrayIndex;
    }

    public boolean isValidNumberOfSpiders(int totalSpiders, int selectSpiders)
    {
        return (totalSpiders >= selectSpiders && totalSpiders <= (selectSpiders * selectSpiders)
                && selectSpiders > 0 && selectSpiders < 317);
    }

    public boolean isValidPower(int spiderPower)
    {
        return (spiderPower >= 1 && spiderPower <= (spidersToSelect * spidersToSelect));
    }

    public void dequeSpiders(int currentSpiders, Queue<Spider> queue, Spider temporaryArray[])
    {
        for (int index = 0; index < currentSpiders; index++)
        {
            temporaryArray[index] = queue.poll();
        }
    }

    public void reduceSpiderPower()
    {
        for (int spiderPosition = 0; spiderPosition < spidersToVisit; spiderPosition++)
        {
            if (spiderPosition != tempIndex)
            {
                if (monk.temporaryArray[spiderPosition].getPower() > 0)
                {
                    monk.temporaryArray[spiderPosition].setPower(monk.temporaryArray[spiderPosition].getPower() - 1);
                }
            }
        }
    }

    public void enqueSpiders()
    {
        for (int spiderPosition = 0; spiderPosition < spidersToVisit; spiderPosition++)
        {
            if (spiderPosition != tempIndex)
            {
                queue.add(temporaryArray[spiderPosition]);
            }
        }
    }

    public static void main(String[] args) throws Exception
    {
        monk = new MonkAndChamberOfSecretsSolution();
        queue = new LinkedList<Spider>();

        monk.getSpiderData();
        for (int spiderIndex = 0; spiderIndex < spidersToSelect; spiderIndex++)
        {
            monk.spidersToVisit = monk.getNumberOfSpidersToSelect();
            monk.dequeSpiders(monk.spidersToVisit, queue, monk.temporaryArray);
            tempIndex = monk.findMaxSpiderPower(monk.temporaryArray);
            monk.reduceSpiderPower();
            monk.enqueSpiders();
        }
    }
}

/**
 * Hold Spider power and its position
 */
class Spider
{

    private int power;
    private int index;

    public Spider(int power, int index)
    {
        this.power = power;
        this.index = index;
    }

    public int getPower()
    {
        return power;
    }

    public void setPower(int power)
    {
        this.power = power;
    }

    public int getIndex()
    {
        return index;
    }
}
