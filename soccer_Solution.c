#include "stdio.h"
#define TEST_CASE_UPPER_LIMIT 100
#define TEST_CASE_LOWER_LIMIT 0
#define NUBER_OF_PASSES_LOWER_LIMIT 1
#define NUBER_OF_PASSES_UPPER_LIMIT 100000 
#define CURRENT_PLAYER_LOWER_LIMIT 1
#define CURRENT_PLAYER_UPPER_LIMIT 100000
int validate(int ,int ,int );									//prototype declaration for validate function

int main()
  {
	int loop_counter,test_case,number_of_passes,current_player,previous_player;
	char pass_type;
	scanf("%d",&test_case);									//For Taking the test cases from the user
	for (loop_counter = 0; loop_counter < test_case; ++loop_counter)
	   {
		scanf("%d",&number_of_passes);							//For taking number of passes
		scanf("%d",&current_player);							//Taking the id of current player
		if(!validate(test_case,number_of_passes,current_player))
		   {
			return 1;
		   }
		while(number_of_passes--)
		   {
			scanf(" %c",&pass_type);
			switch(pass_type)
			   {
				case 'P':
					previous_player=current_player;
					scanf("%d",&current_player);
				break;
				case 'B':
					current_player=current_player+previous_player;		//
					previous_player=current_player-previous_player;		//	Swapping logic
		                	current_player=current_player-previous_player;		//
				break;
                		default:
					return 1;
                		break;
			   }
		  }
		printf("Player %d\n",current_player);						//prints the player having ball at the last	
	   }
	return 0;
  }

int validate(int test_case,int number_of_passes,int current_player)				//Function checks for the constraints max or min value
   {
	/**
	*	return 1 when all condition satisfies else return 0
	**/
	return ((TEST_CASE_LOWER_LIMIT<=test_case&&test_case<=TEST_CASE_UPPER_LIMIT)
				&&(NUBER_OF_PASSES_LOWER_LIMIT<=number_of_passes&&number_of_passes<=NUBER_OF_PASSES_UPPER_LIMIT)
				&&(CURRENT_PLAYER_LOWER_LIMIT<=current_player&&current_player<=CURRENT_PLAYER_UPPER_LIMIT));     
   }