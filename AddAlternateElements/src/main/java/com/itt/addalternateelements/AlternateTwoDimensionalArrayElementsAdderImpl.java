/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.addalternateelements;

/**
 *
 * @author kratiyag.gupta
 */
public class AlternateTwoDimensionalArrayElementsAdderImpl implements AlternateTwoDimensionalArrayElementsAdder {

    private int array[][];
    private int evenSum;
    private int oddSum;
    private int ARRAY_X = 3;
    private int ARRAY_Y = 3;

    public AlternateTwoDimensionalArrayElementsAdderImpl() {
    }

    public AlternateTwoDimensionalArrayElementsAdderImpl(int array[][]) {
        this.array = array;
    }

    public int[][] getArray() {
        return array;
    }

    public void setArray(int[][] array) {
        this.array = array;
    }

    public int getEvenSum() {
        return evenSum;
    }

    public int getOddSum() {
        return oddSum;
    }

    @Override
    public void addAlternateElements() {
       throw new UnsupportedOperationException("Not Yet Implemented");
    }
    
}