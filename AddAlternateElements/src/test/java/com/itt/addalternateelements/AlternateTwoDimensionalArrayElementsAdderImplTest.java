
package com.itt.addalternateelements;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 *
 * @author kratiyag.gupta
 */
public class AlternateTwoDimensionalArrayElementsAdderImplTest {

    int[][] testArray;
    int evenSum;
    int oddSum;
    AlternateTwoDimensionalArrayElementsAdderImpl testInstance;
    
    public AlternateTwoDimensionalArrayElementsAdderImplTest() {
    }

    @Before
    public void setUp() {
        int[][] testArray = { {1, 2, 3},
                              {4, 5, 6},
                              {7, 8, 9}
                            };
        this.evenSum = 25;
        this.oddSum = 20;
        this.testArray = testArray;
        testInstance = new AlternateTwoDimensionalArrayElementsAdderImpl();
    }

    @Test
    public void testGetArray() throws NoSuchFieldException, 
            IllegalArgumentException, IllegalAccessException {
        //Arrange
        final Field array = testInstance.getClass().getDeclaredField("array");
        array.setAccessible(true);
        array.set(testInstance, testArray);
        int[][] expResult = testArray;
        
        //Act
        int[][] result = testInstance.getArray();
        
        //Assert
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testSetArray() throws NoSuchFieldException, 
            IllegalArgumentException, IllegalAccessException {
        //Arrange
        final Field array = testInstance.getClass().getDeclaredField("array");
        array.setAccessible(true);
        
        //Act
        testInstance.setArray(testArray);
        
        //Assert
        assertEquals(testArray, array.get(testInstance));
    }

    @Test
    public void testGetEvenSum() throws Exception {
        
         //Arrange
        final Method addAlternateElements = 
                testInstance.getClass().getMethod("addAlternateElements");
        final Field array = testInstance.getClass().getDeclaredField("array");
        array.setAccessible(true);
        array.set(testInstance, testArray);
        addAlternateElements.invoke(testInstance);
        int expResult = evenSum ;
        
        //Act
        int result = testInstance.getEvenSum();
        
        //Assert
        assertEquals(expResult, result);
    }

    @Test
    public void testGetOddSum() throws Exception {
        
        //Arrange
        final Method addAlternateElements =
                testInstance.getClass().getMethod("addAlternateElements");
        final Field array = testInstance.getClass().getDeclaredField("array");
        array.setAccessible(true);
        array.set(testInstance, testArray);
        
        //Act
        addAlternateElements.invoke(testInstance);
        
        //Assert
        assertEquals(oddSum, testInstance.getOddSum());
    }

    @Test
    public void testAddAlternateElements() throws Exception{
        
        //Arrange
        final Field evenSum = testInstance.getClass().getDeclaredField("evenSum");
        final Field oddSum = testInstance.getClass().getDeclaredField("oddSum");
        final Field array = testInstance.getClass().getDeclaredField("array");
        array.setAccessible(true);
        evenSum.setAccessible(true);
        oddSum.setAccessible(true);
        array.set(testInstance, testArray);
        
        //Act
        testInstance.addAlternateElements();
        
        //Assert
        assertEquals(this.evenSum, evenSum.get(testInstance));
        assertEquals(this.oddSum, oddSum.get(testInstance));
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    @Test
    public void testAddAlternateElements_NullPointerExceptionTest() throws Exception{
        
        //Arrange
        thrown.expect(NullPointerException.class);
       // thrown.expect(ArrayIndexOutOfBoundsException.class);
        final Field array = testInstance.getClass().getDeclaredField("array");
        final Field evenSum = testInstance.getClass().getDeclaredField("evenSum");
        final Field oddSum = testInstance.getClass().getDeclaredField("oddSum");
        array.setAccessible(true);
        evenSum.setAccessible(true);
        oddSum.setAccessible(true);
        array.set(testInstance, null);
        
        //Act
        testInstance.addAlternateElements();
        
        //Assert
        assertEquals(this.evenSum, evenSum.get(testInstance));
        assertEquals(this.oddSum, oddSum.get(testInstance));
    }
    
    @Test
    public void testAddAlternateElements_ArrayIndexOutOfBoundsExceptionTest() throws Exception{
        
        //Arrange
	int[][] testArray = { {1, 2, 3},
                              {4, 5, 6},
                              {7,10}
                            };
        thrown.expect(ArrayIndexOutOfBoundsException.class);
        final Field array = testInstance.getClass().getDeclaredField("array");
        array.setAccessible(true);
        array.set(testInstance, testArray);
        
        //Act
        testInstance.addAlternateElements();
        
    }
}
