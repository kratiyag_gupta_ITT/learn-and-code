/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package customhashtable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author kratiyag.gupta
 */
public class HashTableImpl<K, V> implements HashTable<K, V>
{

    K key;
    V value;
    long bucketSize;
    ArrayList<LinkedList<HashNode<K, V>>> hashBucket;

    public HashTableImpl(int requestedBucketSize)
    {
        bucketSize = requestedBucketSize;
        hashBucket = new ArrayList<LinkedList<HashNode<K, V>>>(requestedBucketSize);
        for (int i = 0; i < requestedBucketSize; i++)
        {
            hashBucket.add(i, null);
        }
    }

    @Override
    public HashNode<K, V> get(K key)
    {
        long hashCode = Math.abs(key.hashCode());
        int index = getIndex(hashCode);
        if (hashBucket.get(index).size() == 1)
        {
            LinkedList<HashNode<K, V>> linkList = hashBucket.get(index);
            HashNode<K, V> hashNode = linkList.get(0);
            return hashNode;
        }
        else
        {
            return getElementFromList(hashBucket.get(index), hashCode);
        }
    }

    @Override
    public void put(K key, V value)
    {
        long hashCode = Math.abs(key.hashCode());
        int index = getIndex(hashCode);
        HashNode<K, V> hashNode = new HashNode<K, V>(key, value);
        if (hashBucket.get(index) != null)
        {
            resolveConflicts(hashNode, index);
        }
        else
        {
            LinkedList<HashNode<K, V>> linkList = new LinkedList<HashNode<K, V>>();
            linkList.add(hashNode);
            hashBucket.set(index, linkList);
        }
    }

    @Override
    public int getIndex(long hashValue)
    {
        return (int) (Math.abs(hashValue) % bucketSize);
    }

    public void resolveConflicts(HashNode<K, V> hashNode, int index)
    {
        LinkedList<HashNode<K, V>> linkList = hashBucket.get(index);
        linkList.add(hashNode);
    }

    public HashNode<K, V> getElementFromList(LinkedList<HashNode<K, V>> linkList, long hashCode)
    {
        Iterator<HashNode<K, V>> iterator = linkList.iterator();
        while (iterator.hasNext())
        {
            HashNode<K, V> hashNode = (HashNode<K, V>) iterator;
            if (Math.abs(hashNode.getHashCode()) == hashCode)
            {
                return hashNode;
            }

        }
        return null;
    }

}
