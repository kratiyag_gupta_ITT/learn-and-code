/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package customhashtable;

/**
 *
 * @author kratiyag.gupta
 */
public class HashNode<K, V>
{

    private K key;
    private V value;
    private long hashCode;

    public HashNode()
    {
    }

    public HashNode(K key, V value)
    {
        this.key = key;
        this.value = value;
        hashCode = key.hashCode();
    }

    public K getKey()
    {
        return key;
    }

    public void setKey(K key)
    {
        this.key = key;
    }

    public V getValue()
    {
        return value;
    }

    public void setValue(V value)
    {
        this.value = value;
    }

    public long getHashCode()
    {
        return hashCode;
    }

    public void setHashCode(long hashCode)
    {
        this.hashCode = hashCode;
    }

}
