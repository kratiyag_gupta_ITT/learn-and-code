/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package customhashtable;

/**
 *
 * @author kratiyag.gupta
 */
public class CustomHashTable
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {

        HashTable<String, String> hashTable = new HashTableImpl<String, String>(10);
        hashTable.put("kratiyag", "hello");
        hashTable.put("Gaurav", "welcome");
        hashTable.put("piyush", "Sayonaraa");
        HashNode<String, String> hashNode = new HashNode<String, String>();
        hashNode = hashTable.get("kratiyag");
        System.out.println(hashNode.getValue());
        hashNode = hashTable.get("Gaurav");
        System.out.println(hashNode.getValue());
        hashNode = hashTable.get("piyush");
        System.out.println(hashNode.getValue());
    }

}
