/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package customhashtable;

/**
 *
 * @author kratiyag.gupta
 */
public interface HashTable <K, V>
{
    public HashNode<K, V> get(K key);
    public void put(K key, V value);
    public int getIndex(long hashValue);
    
}
