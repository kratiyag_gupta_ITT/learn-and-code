/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class MonkAndChamberOfSecrets
{

    int spidersInputArray[] = new int[2];
    int totalSpiders = 0;
    Queue<Spider> spidersHolder = new LinkedList<Spider>();

    MonkAndChamberOfSecrets() throws NumberFormatException, IOException
    {
        int spiderPowerIndex = 1;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        for (String spiderInput : bufferedReader.readLine().split(" "))
        {
            spidersInputArray[totalSpiders++] = Integer.parseInt(spiderInput);
        }

        for (String power : bufferedReader.readLine().split(" "))
        {
            Spider spider = new Spider();
            spider.setIndex(spiderPowerIndex++);
            spider.setPower(Integer.parseInt(power));
            spidersHolder.add(spider);
        }
    }

    public static void main(String[] args) throws NumberFormatException, IOException
    {
        MonkAndChamberOfSecrets monk = new MonkAndChamberOfSecrets();

        for (int index = 0; index < monk.spidersInputArray[1]; index++)
        {
            Queue<Spider> selectedSpiders = monk.dequeSpiders(monk.spidersHolder);
            Spider maxPowerSpider = monk.getMaxPowerSpider(selectedSpiders);
            monk.removeMaxPowerSpider(maxPowerSpider, selectedSpiders);
            monk.decreseSpiderPower(selectedSpiders);
            monk.enqueSpider(selectedSpiders);
        }
    }

    private void enqueSpider(Queue<Spider> selectedpowers)
    {
        spidersHolder.addAll(selectedpowers);
    }

    private void decreseSpiderPower(Queue<Spider> selectedSpiders)
    {
        for (Spider spider : selectedSpiders)
        {
            if (spider.getPower() - 1 >= 0)
            {
                spider.setPower(spider.getPower() - 1);
            }
        }
    }

    private Queue<Spider> removeMaxPowerSpider(Spider MaxPowerSpider, Queue<Spider> selectedSpiders)
    {
        System.out.print(MaxPowerSpider.getIndex() + " ");
        selectedSpiders.remove(MaxPowerSpider);
        return selectedSpiders;

    }

    private Spider getMaxPowerSpider(Queue<Spider> selectedpowers)
    {
        int maxValue = -1;
        Spider maxSpider = null;
        Iterator<Spider> iterator = selectedpowers.iterator();
        while (iterator.hasNext())
        {
            Spider currentSpider = iterator.next();
            if (currentSpider.getPower() > maxValue)
            {
                maxSpider = currentSpider;
                maxValue = currentSpider.getPower();
            }
        }
        return maxSpider;
    }

    private Queue<Spider> dequeSpiders(Queue<Spider> spidersHolder)
    {
        Queue<Spider> tempArrayForSpiders = new LinkedList<Spider>();
        int spidersToSelect = spidersInputArray[1];
        if (spidersHolder.size() <= spidersInputArray[1])
        {
            spidersToSelect = spidersHolder.size();
        }
        for (int i = 0; i < spidersToSelect; i++)
        {
            tempArrayForSpiders.add(spidersHolder.poll());
        }
        return tempArrayForSpiders;
    }
}

class Spider
{

    int power;
    int index;

    public int getPower()
    {
        return power;
    }

    public void setPower(int power)
    {
        this.power = power;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }
}
