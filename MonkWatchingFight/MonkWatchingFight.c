#include <stdio.h>
#include<stdlib.h>

struct node
{
    int data;
    struct node *left_child;
    struct node *right_child;
};

struct node *inserting_element_in_tree(struct node *root, int element)
{
	if (root == NULL)
	{
		struct node *first_element_insertion_as_root = (struct node *)malloc(sizeof(struct node));
		first_element_insertion_as_root->data = element;
		first_element_insertion_as_root->left_child = NULL;
		first_element_insertion_as_root->right_child = NULL;
		return first_element_insertion_as_root;
	}
	else if (element <= root->data)
		root->left_child = inserting_element_in_tree(root->left_child, element);
	else if (element>root->data)
		root->right_child = inserting_element_in_tree(root->right_child, element);

	return root;
}

int max_node_height( int left_child_height, int right_child_height)
{
    if(left_child_height > right_child_height)
    return left_child_height;
    else
    return right_child_height;
}

int find_tree_height(struct node *ptr)
{
	int left_node_height = 0, right_node_height = 0;
	if (ptr == NULL)
		return 0;
	left_node_height = find_tree_height(ptr->left_child);
	right_node_height = find_tree_height(ptr->right_child);
	return max_node_height(left_node_height, right_node_height) + 1;
}


int main() {
    
    struct node *root=NULL;
    int number_of_elements,index_of_element,element_to_be_inserted,height_of_tree;
	scanf("%d",&number_of_elements);
	
	for(index_of_element=0;index_of_element<number_of_elements;index_of_element++)
	{
	    scanf("%d",&element_to_be_inserted);
	    root=inserting_element_in_tree(root,element_to_be_inserted);
	}
	
	height_of_tree=find_tree_height(root);
	printf("%d",height_of_tree);
	return 0;
}
