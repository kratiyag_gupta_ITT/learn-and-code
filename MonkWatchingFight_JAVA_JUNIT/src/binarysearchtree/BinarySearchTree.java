package binarysearchtree;

import java.util.Objects;

/**
 *
 * @author kratiyag.gupta
 */
public class BinarySearchTree {

    private Node root;
    private int array[];
    private int treeHeight;

    public BinarySearchTree() {
        root = null;
    }

    public BinarySearchTree(int array[]) {
        this.array = array;
        root = null;
        generateTree();
        this.treeHeight = getTreeHeight(root);
    }

    public Node getRoot() {
        return root;
    }

    public int getTreeHeight() {
        return treeHeight;
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    private void generateTree() {
        int arrayLength = array.length;
        int index = 0;
        while (index < arrayLength) {
            root = insertNode(root, array[index]);
            index++;
        }
    }
    
    private Node insertNode(Node node, int value) {
        if (Objects.isNull(node)) {
            return new Node(value);
        }
        if (node.getValue() >= value) {
            node.setLeftNode(insertNode(node.getLeftNode(), value));
        }
        else if(node.getValue() < value){
            node.setRightNode(insertNode(node.getRightNode(), value));
        }
        return root;
    }

    private int getTreeHeight(Node root) {
        if (root == null) {
            return 0;
        }
        return 1 + Math.max(getTreeHeight(root.getRightNode()), getTreeHeight(root.getLeftNode()));
    }
}
