/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarysearchtree;

import java.util.Scanner;

/**
 *
 * @author kratiyag.gupta
 */
public class MonkWatchingFight {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        MonkWatchingFight binarySearchTreeImpl = new MonkWatchingFight();
        int arrayLength;
        arrayLength = in.nextInt();
        int array[] = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            array[i] = in.nextInt();
            if (!binarySearchTreeImpl.isValidInput(arrayLength, array[i])) {
                break;
            }
        }
        BinarySearchTree bst = new BinarySearchTree(array);
        System.out.println(bst.getTreeHeight());
    }

    private boolean isValidInput(int totalElements, int value) {
        return ((totalElements > 0 && totalElements < 1000) && (value > 0 && value < 1000000));
    }

}
