/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarysearchtree;

/**
 *
 * @author kratiyag.gupta
 */
public class Node
{
  private int value;
  private int level;
  private Node leftNode;
  private Node rightNode;

    public Node()
    {
    }
    public Node(int value)
    {
        this.value = value;
    }
    public int getValue()
    {
        return value;
    }

    public void setValue(int value)
    {
        this.value = value;
    }

    public Node getLeftNode()
    {
        return leftNode;
    }

    public void setLeftNode(Node leftNode)
    {
        this.leftNode = leftNode;
    }

    public Node getRightNode()
    {
        return rightNode;
    }

    public void setRightNode(Node rightNode)
    {
        this.rightNode = rightNode;
    }

    public int getLevel()
    {
        return level;
    }

    public void setLevel(int level)
    {
        this.level = level;
    }
  
}
