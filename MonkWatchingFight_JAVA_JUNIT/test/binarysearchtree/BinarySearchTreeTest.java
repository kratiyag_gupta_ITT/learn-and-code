/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package binarysearchtree;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kratiyag.gupta
 */
public class BinarySearchTreeTest {
    
    static int array[] ;
    static int treeHeight;
    static Node rootNode;
    public BinarySearchTreeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        int[] localArray = {2, 1, 3, 4};
        BinarySearchTreeTest.array = localArray;
        BinarySearchTree binarySearchTree = new BinarySearchTree(localArray);
        treeHeight = binarySearchTree.getTreeHeight();
        rootNode = binarySearchTree.getRoot();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     * Test of getRoot method, of class BinarySearchTree.
     */
    @Test
    public void testGetRoot() {
        System.out.println("getRoot");
        BinarySearchTree instance = new BinarySearchTree(BinarySearchTreeTest.array);
        Node expResult = rootNode;
        Node result = instance.getRoot();
        assertEquals(expResult.getValue(), result.getValue());
    }

    /**
     * Test of getTreeHeight method, of class BinarySearchTree.
     */
    @Test
    public void testGetTreeHeight() {
        System.out.println("getTreeHeight");
        BinarySearchTree instance = new BinarySearchTree(BinarySearchTreeTest.array);
        int expResult = BinarySearchTreeTest.treeHeight;
        int result = instance.getTreeHeight();
        assertEquals(expResult, result);
    }

    /**
     * Test of getArray method, of class BinarySearchTree.
     */
    @Test
    public void testGetArray() {
        System.out.println("getArray");
        BinarySearchTree instance = new BinarySearchTree(BinarySearchTreeTest.array);
        int[] expResult = BinarySearchTreeTest.array;
        int[] result = instance.getArray();
        assertArrayEquals(expResult, result);
    }
    
}
