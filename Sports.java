
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Scanner;

public class Sports
{
	int maxValue = 0;
    String maxValSportName="";
	HashSet<String> personHolder;
    Hashtable<String, Integer> sportsHolder;
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        personHolder = new HashSet<String>();
		sportsHolder = new Hashtable<String, Integer>();
        int numberOfPeople = in.nextInt();
        for (int i = 0; i < numberOfPeople; i++)
        {
            String personName = in.next();
            String sportsName = in.next();
            calculateSportsData(personName, sportsName);
        }
        System.out.println(maxValSportName);
        System.out.println(sportsHolder.get("football")==null ? 
                                    "0":sportsHolder.get("football").intValue());
    }
	
	public void calculateSportsData(String personName, String sportsName)
	{
		if (!personHolder.contains(personName))
            {
                personHolder.add(personName);
                if(sportsHolder.containsKey(sportsName)) {
                    int value = sportsHolder.get(sportsName)+1;
                    sportsHolder.put(sportsName, value);
                    if(value >= maxValue) {
                        maxValue = value;
                        maxValSportName = sportsName;
                    }
                }
                else {
                    sportsHolder.put(sportsName, 1);
                }
            }
	}
}